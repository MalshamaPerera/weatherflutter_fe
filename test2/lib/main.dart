import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


void main() => runApp(MyApp());


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final appTitle = 'Weather App';


    return MaterialApp(

      title: appTitle,
      theme: ThemeData(

        brightness: Brightness.dark,

        fontFamily: 'Georgia',

      ),


      //child: image,

      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,

          title: Text(appTitle, style: TextStyle(fontSize: 25 , fontStyle: FontStyle.italic,fontWeight: FontWeight.bold)),


          backgroundColor: Colors.deepPurpleAccent,



        ),
        bottomNavigationBar: BottomAppBar(
          child: Row( children : <Widget>
          [
            Text("Wether Details", style: TextStyle(fontSize: 15, )),
            Text("@All Rights reserved \n"),

          ],
          ),
          color: Colors.deepPurpleAccent,
        ),
        body: Center(
          child :MyCustomForm(),

        ),


      ),

    );

  }
}

// Form widget
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {

  final _formKey = GlobalKey<FormState>();


  String city="";
  TextEditingController cityName = new TextEditingController();


  @override
  Widget build(BuildContext context) {

    return Form(

      key: _formKey,
      child: Column(

        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[



          TextFormField(
            controller: cityName,
            decoration: InputDecoration(
              icon: const Icon(Icons.location_city),
              hintText: 'Enter City',
            ),
          ),

          Padding(

            padding: const EdgeInsets.only(left: 150.0, top: 40.0),
            child: RaisedButton(


              onPressed: () async  {
                setState(() {

                  city = cityName.text;
                });

                try {

                  var client = new HttpClient();


                  var uri = Uri.parse("http://192.168.8.103:2000?city_name=$city");

                  var request = await client.getUrl(uri);
                  var response = await request.close();
                  var responseBody = await response.transform(utf8.decoder).join();
                  final object = json.decode(responseBody);
                 var prettyString = JsonEncoder.withIndent("  ").convert(object);
                  var weather = object['weather'];
                  var main = object['main'];
                  var visibility=object['visibility'];
                  var sys=object['sys'];
                  //print("weather :  $weather");
//                  print("Temp :  $main");
//                  print("Visibility : $visibility");
//                  print("Country : $sys");

                  Map result=object;

                  print("\n Coord : ${result['coord']['lon']}"+"\n Temperature : ${result['main']['temp']}"+"\n Visibility : ${result['visibility']}"+"\n Clouds : ${result['clouds']['all']}"+"\n Country : ${result['sys']['country']}"+"\n TimeZone : ${result['timezone']}");



                  return showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(

                        content: (
                            Text("Coord : ${result['coord']['lon']}"+"\n Temperature : ${result['main']['temp']}"+"\n Visibility : ${result['visibility']}"+"\n Clouds : ${result['clouds']['all']}"+"\n Country : ${result['sys']['country']}"+"\n TimeZone : ${result['timezone']}")

                        ),

                      );
                    },
                  );


                } catch (exception) {
                  print(exception);
                  print("not working");
                }

              },

              child: Text('Search'),


            ),

          ),


        ],

      ),

    );

  }
}